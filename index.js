const { parse } = require('path');
const { generateRandomId } = require('./functions');

const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, { pingInterval: 500 });

/* General utilities
----------------------------------- */

function parseData(data){
  try{
    if (typeof data == "string"){
      data = JSON.parse(data);
    }

  }catch(e){}
  return data;
}

/* Game volatil store
----------------------------------- */
const sessions = {};
const users = {};
const rooms = {};

/* Room pair/creation
----------------------------------- */

function generateUniqueRoomId(){
  do{   
    const roomId = generateRandomId();
     if (!rooms.hasOwnProperty(roomId)){
        return roomId;
     }
  }while(true);
}
function registerUser(sessionId, id, name){
  if (!sessions[sessionId]){
    sessions[sessionId] = {
      userId: id,
      rooms: []
    };
  }

  users[id] = {
    id,
    name
  };
}

function createRoom(sessionId, roomId, user){
  registerUser(sessionId, user.id, user.name);
  sessions[sessionId].rooms.push(roomId);
  rooms[roomId] = {
      roomId: roomId,
      player1: user.id,
      player2: null,
      fights: {}
  };
}
function joinRoom(sessionId, roomId, user){
  console.log("Looking for room: " + roomId);
  if (rooms[roomId]){   
    const room = rooms[roomId];
    if (!room.player2){
      registerUser(sessionId, user.id, user.name);
      sessions[sessionId].rooms.push(roomId);
      room.player2 = user.id;
      const fight = generateRoomFight(room);
      return {success: true, room, fight};
    }else{
      return {
        success: false,
        message: 'La sala está ocupada'
      }
    }
  }

  return {
    success: false,
    message: 'La sala no existe'
  }

}

function parseRoomInfo(room){
  return {
    roomId: room.roomId,
    player1: users[room.player1],
    player2: users[room.player2],
  }
}

function cancelRoom(roomId){
  delete rooms[roomId];
}


/* Fight related
----------------------------------- */

function generateRoomFight(room){
  const fight = generateFight();
  room.fights[fight.id] = fight;
  return fight;
}

function generateFight(){

  return {
    id: (new Date()).getTime(),
    player1: {
      score: -1,
      rematch: false
    },
    player2:{
      score: -1,
      rematch: false
    }
  }
  
}

function getPlayerKey(room, userId){
  if (room.player1 === userId){
    console.log('compared: ', room.player1, userId);
    return 'player1';
  }else if (room.player2 === userId){
    return 'player2';
  }
  return null;
}

function setPlayerScore(roomId, fightId, playerKey, score){
  const room = rooms[roomId];
  const fight = room.fights[fightId];
  fight[playerKey].score = score;
  return fight;
}

function setPlayerRematch(roomId, fightId, playerKey, rematch){
  const room = rooms[roomId];
  const fight = room.fights[fightId];
  fight[playerKey].rematch = rematch;
  return fight;
}



io.on('connection', (socket) => {

    console.log('New user connection!');

    /* ROOM JOIN / CREATION
    --------------------------------- */

    socket.on('create-room', (data) => {
        try{

          data = parseData(data);

          const user = data.user;
          // Generate random room id
          const roomId = generateUniqueRoomId().toString();
          createRoom(socket.id, roomId, user);
          socket.join(roomId);
          console.log('Created room!', roomId);
          io.to(roomId).emit('created-room', {
            roomId: roomId
          });
      }catch(e){
        console.log('Failed to create room: ', e, data);
      }
    });

    socket.on('join-room', (data) => {

      try{

        data = parseData(data);

        const user = data.user;
        const roomId = data.roomId;
        const response = joinRoom(socket.id, roomId, user);

        if (response.success){
          
          socket.join(roomId);

          console.log('Starting game!', roomId)

          io.to(roomId).emit('start-game', {
            room: parseRoomInfo(response.room),
            fight: response.fight
          });

        }else{

          socket.emit('room-failed', {
            message: response.message
          })

        }

      }catch(e){
        console.log('Failed to join room', e);
      }

    });

    socket.on('send-score', (data) => {
      try{
        
        data = parseData(data);

        const { userId, roomId, fightId, score } = data;
        const room = rooms[roomId];
        const playerKey = getPlayerKey(room, userId);
        if (!playerKey || !room) return socket.emit('invalid-action');
        const fight = setPlayerScore(roomId, fightId, playerKey, score);
        const isFinished = (fight.player1.score !== -1 && fight.player2.score !== -1);
        if (isFinished){
          io.to(roomId).emit('end-fight', {
            room: parseRoomInfo(room),
            fight: fight
          });
        }
      }catch(e){
        console.log('Failed to send score', e);
      }
    });

  socket.on('send-rematch', (data) => {
    
    data = parseData(data);

    try{
      const { userId, roomId, fightId, rematch } = data;
      const room = rooms[roomId];
      const playerKey = getPlayerKey(room, userId);
      if (!playerKey || !room) return socket.emit('invalid-action');
      const fight = setPlayerRematch(roomId, fightId, playerKey, rematch);
      if (!rematch){
        socket.to(roomId).emit('game-ended');
        cancelRoom(roomId);
      }else{
        const newFight = generateRoomFight(room);
        const isFinished = (fight.player1.rematch && fight.player2.rematch);
        if (isFinished){
          io.to(roomId).emit('game-restart', {
            room: parseRoomInfo(room),
            fight: newFight
          });
        }
      }
    }catch(e){
      console.log('Failed to rematch: ', e);
    }
  });

  socket.on('disconnecting', () => {
    try{
      const sessionId = socket.id;
      const session = sessions[sessionId];
      if (session){
        session.rooms.map((roomId) => {
            io.to(roomId).emit('game-leave');
        });
      }
      delete sessions[sessionId];
      console.log('Deleted sessions');
    }catch(e){
        console.log('Failed to disconnect: ', e);
    }
  });

  socket.on('disconnect', () => {
    console.log('A user is disconnected!')
  });

});



/*app.get('/', (req, res) => {
  res.send('<h1>Hello world</h1>');
});*/

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/socket.io.min.js', (req, res) => {
  res.sendFile(__dirname + '/socket.io.min.js');
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});